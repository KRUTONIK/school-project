import flask
import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import time
import shutil


TEMPLATE_FOLDER = os.path.abspath("templates")
STATIC_FOLDER = os.path.abspath("static")


def makefloat(x):
    try:
        float(x)
    except ValueError:
        return 0
    return x


app = flask.Flask(__name__)


@app.route("/")
def main():
    return flask.render_template("main.html")


@app.route("/linear", methods=["GET", "POST"])
def linear():
    x = ""
    jpg = ""
    shutil.rmtree(str(os.getcwd()) + "/mysite/static/graph/")
    os.mkdir(str(os.getcwd()) + "/mysite/static/graph/")

    if flask.request.method == "POST":
        k = float(flask.request.form["k"])
        b = float(flask.request.form["b"])
        if k != 0:
            x = -b / k
        elif k == 0 and b != 0:
            x = "не существует"
        elif k == 0 and b == 0:
            x = "бесконечное множество решений"

        ax = plt.gca()
        ax.clear()
        if k != 0:
            plt.plot([(-b / k) - 5, 0, (-b / k) + 5], [k*(-b / k - 5) + b, b, k*(-b / k + 5) + b], color='#000080')
        elif k == 0 and b != 0:
            plt.plot([-5, 0, 5], [b, b, b], color='#000080')
        elif k == 0 and b == 0:
            plt.plot([-5, 0, 5], [0, 0, 0], color='#000080')
        ax.axhline(y=0, color='k')
        ax.axvline(x=0, color='k')
        plt.ioff()
        jpg = str(time.time_ns())
        linearjpg = str(os.getcwd()) + "/mysite/static/graph/{0}.jpg".format(jpg)
        plt.savefig(linearjpg)


    return flask.render_template("linear.html", x=x, jpg=jpg)


@app.route("/linear/graph/<int:jpg>", methods=["GET"])
def graphlinear(jpg):
    src = "/{0}.jpg".format(jpg)
    return flask.render_template("lineargraph.html", jpg=src)


@app.route("/quadraticequation", methods=["GET", "POST"])
def quadraticequation():
    x1 = ""
    x2 = ""
    discriminant = ""
    jpg = ""
    shutil.rmtree(str(os.getcwd()) + "/mysite/static/graph/")
    os.mkdir(str(os.getcwd()) + "/mysite/static/graph/")

    if flask.request.method == "POST":
        a = float(flask.request.form["a"])
        b = float(flask.request.form["b"])
        c = float(flask.request.form["c"])
        if a != 0 and b != 0 and c != 0:
            discriminant = (b * b) - (4 * a * c)
            if discriminant < 0:
                x1 = "не существует"
                x2 = "не существует"
            elif discriminant == 0:
                x1 = -b / (2 * a)
                x2 = "не существует"
            else:
                x1 = (-b + (discriminant ** 0.5)) / (2 * a)
                x2 = (-b - (discriminant ** 0.5)) / (2 * a)
        elif a == 0 and b == 0 and c == 0:
            x1 = "бесконечное множество решений"
            x2 = "бесконечное множество решений"
        elif a != 0 and b != 0 and c == 0:
            x1 = 0
            x2 = -b / a
        elif a != 0 and b == 0 and c != 0:
            if -c / a < 0:
                x1 = "не существует"
                x2 = "не существует"
            else:
                x1 = (-c / a) ** 0.5
                x2 = -((-c / a) ** 0.5)
        elif a == 0 and b != 0 and c != 0:
            x1 = -c / b
            x2 = "не существует"
        elif a != 0 and b == 0 and c == 0:
            x1 = 0
            x2 = "не существует"
        elif a == 0 and b != 0 and c == 0:
            x1 = 0
            x2 = "не существует"
        elif a == 0 and b == 0 and c != 0:
            x1 = "не существует"
            x2 = "не существует"

        if x1 == "не существует" and x2 == "не существует":
            if a != 0:
                x = np.linspace(min(0, (-b / (2 * a)) - 1), max(0, (-b / (2 * a)) + 1), 10000)
            else:
                x = np.linspace(min(0, -b - 1), max(0, -b + 1), 10000)
        else:
            x = np.linspace(min(makefloat(x1), 0) - 1, max(makefloat(x2), 0) + 1, 10000)
        y = a*(x**2) + b*x + c

        fig, ax = plt.subplots()
        ax.clear()
        ax.plot(x, y, color='#000080')
        ax.axhline(y=0, color='k')
        ax.axvline(x=0, color='k')
        plt.ioff()
        jpg = str(time.time_ns())
        quadraticequationjpg = str(os.getcwd()) + "/mysite/static/graph/{0}.jpg".format(jpg)
        plt.savefig(quadraticequationjpg)


    return flask.render_template("quadraticequation.html",
                                x1=x1, x2=x2, discriminant=discriminant, jpg=jpg)


@app.route("/quadraticequation/graph/<int:jpg>", methods=["GET"])
def graphquadraticequation(jpg):
    src = "/{0}.jpg".format(jpg)
    return flask.render_template("quadraticequationgraph.html", jpg=src)


@app.route("/biquadraticequation", methods=["GET", "POST"])
def biquadraticequation():
    x1 = ""
    x2 = ""
    x3 = ""
    x4 = ""
    discriminant = ""
    t1 = ""
    t2 = ""
    jpg = ""
    shutil.rmtree(str(os.getcwd()) + "/mysite/static/graph/")
    os.mkdir(str(os.getcwd()) + "/mysite/static/graph/")

    if flask.request.method == "POST":
        a = float(flask.request.form["a"])
        b = float(flask.request.form["b"])
        c = float(flask.request.form["c"])
        if a != 0 and b != 0 and c != 0:
            discriminant = (b * b) - (4 * a * c)
            if discriminant < 0:
                t1 = "не существует"
                t2 = "не существует"
                x1 = "не существует"
                x2 = "не существует"
                x3 = "не существует"
                x4 = "не существует"
            elif discriminant == 0:
                t1 = -b / (2 * a)
                t2 = "не существует"
                if t1 > 0:
                    x1 = t1 ** 0.5
                    x2 = -(t1 ** 0.5)
                elif t1 == 0:
                    x1 = 0
                    x2 = "не существует"
                else:
                    x1 = "не существует"
                    x2 = "не существует"
                x3 = "не существует"
                x4 = "не существует"
            elif discriminant > 0:
                t1 = (-b + (discriminant ** 0.5)) / (2 * a)
                t2 = (-b - (discriminant ** 0.5)) / (2 * a)
                if t1 > 0:
                    x1 = t1 ** 0.5
                    x2 = -(t1 ** 0.5)
                elif t1 == 0:
                    x1 = 0
                    x2 = "не существует"
                else:
                    x1 = "не существует"
                    x2 = "не существует"
                if t2 > 0:
                    x3 = t2 ** 0.5
                    x4 = -(t2 ** 0.5)
                elif t2 == 0:
                    x3 = 0
                    x4 = "не существует"
                else:
                    x3 = "не существует"
                    x4 = "не существует"
        elif a == 0 and b == 0 and c == 0:
            t1 = "бесконечное множество решений"
            t2 = "бесконечное множество решений"
            x1 = "бесконечное множество решений"
            x2 = "бесконечное множество решений"
            x3 = "бесконечное множество решений"
            x4 = "бесконечное множество решений"
        elif a != 0 and b != 0 and c == 0:
            t1 = 0
            t2 = -b / a
            x1 = 0
            x2 = "не существует"
            if t2 < 0:
                x3 = "не существует"
                x4 = "не существует"
            else:
                x3 = t2 ** 0.5
                x4 = -(t2 ** 0.5)
        elif a != 0 and b == 0 and c != 0:
            if -c / a < 0:
                t1 = "не существует"
                t2 = "не существует"
                x1 = "не существует"
                x2 = "не существует"
                x3 = "не существует"
                x4 = "не существует"
            else:
                t1 = (-c / a) ** 0.5
                t2 = -((-c / a) ** 0.5)
                x1 = t1 ** 0.5
                x2 = -(t1 ** 0.5)
                x3 = "не существует"
                x4 = "не существует"
        elif a == 0 and b != 0 and c != 0:
            t1 = -c / b
            t2 = "не существует"
            if t1 < 0:
                x1 = "не существует"
                x2 = "не существует"
            else:
                x1 = t1 ** 0.5
                x2 = -(t1 ** 0.5)
            x3 = "не существует"
            x4 = "не существует"
        elif a != 0 and b == 0 and c == 0:
            t1 = 0
            t2 = "не существует"
            x1 = 0
            x2 = "не существует"
            x3 = "не существует"
            x4 = "не существует"
        elif a == 0 and b != 0 and c == 0:
            t1 = 0
            t2 = "не существует"
            x1 = 0
            x2 = "не существует"
            x3 = "не существует"
            x4 = "не существует"
        elif a == 0 and b == 0 and c != 0:
            t1 = "не существует"
            t2 = "не существует"
            x1 = "не существует"
            x2 = "не существует"
            x3 = "не существует"
            x4 = "не существует"

        if x1 == "не существует" and x2 == "не существует" and x3 == "не существует" and x4 == "не существует":
            if a != 0:
                x = np.linspace(min(0, (-b / (2 * a)) - 5), max(0, (-b / (2 * a)) + 5), 10000)
            else:
                x = np.linspace(min(0, -b - 1), max(0, -b + 1), 10000)
        else:
            x = np.linspace(min(makefloat(x1), makefloat(x2), makefloat(x3), makefloat(x4), 0) - 1, max(makefloat(x1), makefloat(x2), makefloat(x3), makefloat(x4), 0) + 1, 10000)
        y = a*(x**4) + b*(x**2) + c

        fig, ax = plt.subplots()
        ax.clear()
        ax.plot(x, y, color='#000080')
        ax.axhline(y=0, color='k')
        ax.axvline(x=0, color='k')
        plt.ioff()
        jpg = str(time.time_ns())
        biquadraticequationjpg = str(os.getcwd()) + "/mysite/static/graph/{0}.jpg".format(jpg)
        plt.savefig(biquadraticequationjpg)


    return flask.render_template("biquadraticequation.html",
                                x1=x1, x2=x2, x3=x3, x4=x4, discriminant=discriminant, t1=t1, t2=t2, jpg=jpg)


@app.route("/biquadraticequation/graph/<int:jpg>", methods=["GET"])
def graphbiquadraticequation(jpg):
    src = "/{0}.jpg".format(jpg)
    return flask.render_template("biquadraticequationgraph.html", jpg=src)